#!/bin/sh
xrandr --output HDMI-2 --off --output HDMI-1 --off \
  --output DP-1 --mode 1920x1080 --pos 1920x0 --rotate normal -r 60 \
  --output eDP-1 --mode 1920x1080 --pos 0x552 --rotate normal -r 60 \
  --output DP-2 --primary --mode 1920x1080 --pos 3840x0 --rotate normal -r 60

$HOME/.screenlayout/common.sh
